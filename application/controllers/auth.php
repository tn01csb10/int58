<?php
header('Access-Control-Allow-Origin: *');
Class Auth extends CI_Controller {

public function __construct() {
parent::__construct();

// Load form helper library
$this->load->helper('form');

// Load form validation library
$this->load->library('form_validation');

// Load session library
$this->load->library('session');

}

// Show login page
public function index() {
$this->load->view('home');
}

// Show registration page
public function verify_client_login(){
  $this->load->database();
  if(isset($_POST['username']) && isset($_POST['password'])){
    $client_username=$_POST['username'];
    $client_password=md5($_POST['password']);

    $get_category=$this->db->get_where('client_db',array('email' => $client_username, 'password' => $client_password ));
    $num = $get_category->num_rows();
    if($num == 1){
      foreach ($get_category->result() as $row)
      { 
        $username=$row->username;
        $first_name=$row->first_name;
        $last_name=$row->last_name;
        $phone_no=$row->phone_no;
        $email=$row->email;
        $client_id=$row->id;

        $session_data = array(
        'username' => $username,
        'first_name' => $first_name,
        'last_name' => $last_name,
        'phone_no' => $phone_no,
        'email' => $email,
        'client_id'=>$client_id
        );
        $this->session->set_userdata('logged_in', $session_data);
      }
      echo 'true';
    }else{
      echo 'false';
    }
  }
}

public function client_registration(){
  $this->load->database();
  if(isset($_POST['email']) && isset($_POST['password']) && isset($_POST['first_name']) && isset($_POST['last_name']) && isset($_POST['client_no'])){
    $client_email=$_POST['email'];
    $client_password=$_POST['password'];
    $client_no=$_POST['client_no'];
    $first_name=$_POST['first_name'];
    $last_name=$_POST['last_name'];
    $hashed_password=md5($client_password);
    $cipher=rand(444,888).rand(04,111);
    $username=$first_name.$cipher;
    $data = array(
        'username' => $username,
        'first_name' => $first_name,
        'last_name' => $last_name,
        'phone_no' => $client_no,
        'email' => $client_email,
        'password' => $hashed_password
    );

    $this->db->insert('client_db', $data);

     /*$get_category=$this->db->get_where('client_db',array('email' => $client_email, 'password' => $client_password ));
      $num = $get_category->num_rows();
      if($num == 1){
        foreach ($get_category->result() as $row){ 
          
          $client_id=$row->id;

        }*/

     $session_data = array(
        'username' => $username,
        'first_name' => $first_name,
        'last_name' => $last_name,
        'phone_no' => $client_no,
        'email' => $client_email
        );
    $this->session->set_userdata('logged_in', $session_data);
    echo 'true';
   }
 }

}


//$this->session->userdata['logged_in']['username']
?>
