<?php
header('Access-Control-Allow-Origin: *');
defined('BASEPATH') OR exit('No direct script access allowed');
use PHPMailer\PHPMailer\PHPMailer;
		use PHPMailer\PHPMailer\Exception;
		//Load Composer's autoloader
		require 'vendor/autoload.php';
class Dashboard extends CI_Controller {	

	function __construct() {
	parent::__construct();

	$this->load->library('pagination');
	$this->load->helper('form');
	$this->load->library('form_validation');
	$this->load->helper('url');
	$this->load->helper('date');
	}
	 
	public function index()
	{
		$this->load->view('client/client_dashboard');
	}

	public function add_company()
	{
		$this->load->database();
		$this->load->view('client/add_company');
	}

	public function insert_visitor_record()
	{
		$this->load->database();
		if(isset($_POST['f_name']) && isset($_POST['l_name']) && isset($_POST['datepicker']) && isset($_POST['visitor_email']) && isset($_POST['visitor_phone']) && isset($_POST['hidden_id_taker']) && isset($_POST['govt_card']) && isset($_POST['reason']) && isset($_POST['unwind_id']) && isset($_POST['employee_meet'])  && isset($_POST['profile_company_name_b'])  && isset($_POST['profile_dept_name_b']) && isset($_POST['form_data_client_id'])){
		$f_name=$_POST['f_name'];
		$l_name=$_POST['l_name'];
		$datepicker=$_POST['datepicker'];
		$visitor_email=$_POST['visitor_email'];
		$visitor_phone=$_POST['visitor_phone'];
		$hidden_id_taker=$_POST['hidden_id_taker'];
		$govt_card=$_POST['govt_card'];
		$reason=$_POST['reason'];
		$automation_id=$_POST['unwind_id'];
		$employee_meet=$_POST['employee_meet'];
		$profile_company_name_b=$_POST['profile_company_name_b'];
		$profile_dept_name_b=$_POST['profile_dept_name_b'];
		$form_data_client_id=$_POST['form_data_client_id'];
		$visitor_data = array(
	      	'client_id' => $form_data_client_id,
	      	'automation_id' => $automation_id,
	      	'fname' => $f_name,
	      	'lname' => $l_name,
	      	'email' => $visitor_email,
	      	'phone' => $visitor_phone,
	      	'dob' => $datepicker,
	      	'govt_id' => $govt_card,
	      	'id_number' => $hidden_id_taker,
	      	'reason_to_visit' => $reason,
	      	'company_visit' => $profile_company_name_b,
	      	'dept_visit' => $profile_dept_name_b,
	      	'employee_visit' => $employee_meet
	      );
		 $this->db->insert('visitor_details', $visitor_data);
		
		echo 'true' ;
		exit();
		
		}
	}


	public function insert_visitor_record_re()
	{
		$this->load->database();
		if(isset($_POST['visitor_email']) && isset($_POST['unwind_id']) && isset($_POST['employee_meet'])  && isset($_POST['profile_company_name_b'])  && isset($_POST['profile_dept_name_b']) && isset($_POST['form_data_client_id'])){
		
		$visitor_email=$_POST['visitor_email'];
		
		$automation_id=$_POST['unwind_id'];
		$employee_meet=$_POST['employee_meet'];
		$profile_company_name_b=$_POST['profile_company_name_b'];
		$profile_dept_name_b=$_POST['profile_dept_name_b'];
		$form_data_client_id=$_POST['form_data_client_id'];

		$get_category=$this->db->get_where('visitor_details',array('client_id' => $form_data_client_id, 'email' => $visitor_email ));
	    foreach ($get_category->result() as $row)
	    {	
	    	$f_name=$row->fname;
	    	$l_name=$row->lname;
	    	$datepicker=$row->dob;
	    	$visitor_phone=$row->phone;
	    	$hidden_id_taker=$row->id_number;
	    	$govt_card=$row->govt_id;
	    	$reason=$row->reason_to_visit;
	    	
	      }
		$visitor_data = array(
	      	'client_id' => $form_data_client_id,
	      	'automation_id' => $automation_id,
	      	'fname' => $f_name,
	      	'lname' => $l_name,
	      	'email' => $visitor_email,
	      	'phone' => $visitor_phone,
	      	'dob' => $datepicker,
	      	'govt_id' => $govt_card,
	      	'id_number' => $hidden_id_taker,
	      	'reason_to_visit' => $reason,
	      	'company_visit' => $profile_company_name_b,
	      	'dept_visit' => $profile_dept_name_b,
	      	'employee_visit' => $employee_meet
	      );
		 $this->db->insert('visitor_details', $visitor_data);
		
		echo $f_name.' '.$l_name ;
		exit();
		
		}
	}

	public function send_sms_otp()
	{
		$this->load->database();

		if(isset($_POST['visitor_number']) ){
		      $visitor_number=$_POST['visitor_number'];
		      
		      $otp=rand(1111,9999);
		      echo $otp;
		      $curl = curl_init();

		      curl_setopt_array($curl, array(
		        CURLOPT_URL => "http://api.msg91.com/api/v2/sendsms",
		        CURLOPT_RETURNTRANSFER => true,
		        CURLOPT_ENCODING => "",
		        CURLOPT_MAXREDIRS => 10,
		        CURLOPT_TIMEOUT => 30,
		        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		        CURLOPT_CUSTOMREQUEST => "POST",
		        CURLOPT_POSTFIELDS => "{ \"sender\": \"SELSMS\", \"route\": \"4\", \"country\": \"91\", \"sms\": [ { \"message\": \" OTP is ".$otp." \n\", \"to\": [$visitor_number] } ] }",
		        CURLOPT_SSL_VERIFYHOST => 0,
		        CURLOPT_SSL_VERIFYPEER => 0,
		        CURLOPT_HTTPHEADER => array(
		          "authkey: 221747AjNQ78e6qpxs5b2bdbc8",
		          "content-type: application/json"
		        ),
		      ));

		      $response = curl_exec($curl);
		      $err = curl_error($curl);

		      curl_close($curl);

		      if ($err) {
		        //echo "cURL Error #:" . $err;
		      } else {
		        echo $otp;
		      }
		    }else{
		      echo 'no';
		    }
	}

	public function send_mail_otp()
	{
		$this->load->database();
		
		if(isset($_POST['visitor_email']) ){
		      $visitor_email=$_POST['visitor_email'];
		      
		      $otp=rand(1111,9999);
		      //echo $otp;
		      
				// Instantiation and passing `true` enables exceptions
				$mail = new PHPMailer(true);

				try {
				    //Server settings
				    $mail->SMTPDebug = 0;                                       // Enable verbose debug output
				    $mail->isSMTP();                                            // Set mailer to use SMTP
				    $mail->Host       = 'smtp.sendgrid.net';  // Specify main and backup SMTP servers
				    $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
				    $mail->Username   = 'saletancy.data';                     // SMTP username
				    $mail->Password   = 'Saletancy@2017';                               // SMTP password
				    $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
				    $mail->Port       = 587;                                    // TCP port to connect to

				    //Recipients
				    $mail->setFrom('pratikjaiswal830@gmail.com', 'Bind Visitor');
				    $mail->addAddress($visitor_email, 'User');     // Add a recipient
				    
				    // Content
				    $mail->isHTML(true);                                  // Set email format to HTML
				    $mail->Subject = 'Bind Visitor: check in OTP ';
				    $mail->Body    = 'Your verification code is <b>'.$otp.'</b>';
				    //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

				    $mail->send();
				    echo $otp;
				} catch (Exception $e) {
				    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
				}
		     
		    }else{
		      echo 'no';
		    }
	}

	public function add_company_name(){
	$this->load->database(); 
	$client_id=($this->session->userdata['logged_in']['client_id']);
	//$this->load->model('backend');
	if(isset($_POST['category_name'])){
		$category_name=$_POST['category_name'];
		$category = array(
			'client_id' => $client_id,
	      	'company_name' => $category_name
	      );
		$this->db->insert('client_business_details', $category);
		echo 'true' ;
		exit();
	}
 }

public function sub_category_info(){
	$this->load->database(); 
	if(isset($_POST['val'])){
	$val=$_POST['val'];
	$get_category=$this->db->get_where('client_business_details',array('company_name' => $val ));
    foreach ($get_category->result() as $row)
    {	
    	$sub_category=$row->departments;
    	$sub_array=explode(',', $sub_category);
    	$size=sizeof($sub_array);
    	for($i=0;$i<=$size-1;$i++){
    		echo '<li class="list-group-item text-bold">'.$sub_array[$i].'</li>';
    	}
        
      }
	}
 }

 public function number_check(){
	$this->load->database(); 
	if(isset($_POST['visitor_number']) && isset($_POST['client_id'])){
	$visitor_number=$_POST['visitor_number'];
	$client_id=$_POST['client_id'];
	$get_category=$this->db->get_where('visitor_details',array('client_id' => $client_id, 'email' => $visitor_number ));
    $numq= $get_category->num_rows();
      if($numq > 0){
      	echo 'yes';
        exit();
      }else{
      	echo 'no';
      	exit();
      }
	}
 }

 public function employee_data(){
		$this->load->database();
		if($_FILES["select_file"]["name"] != '')
		{
		 $test = explode('.', $_FILES["select_file"]["name"]);
		 $ext = end($test);
		 $name = rand(10000000, 99999999).'.'.$ext;
		 $location = $_SERVER['DOCUMENT_ROOT'].'/int58/assets/img/client/employees/'.$name;  
		 move_uploaded_file($_FILES["select_file"]["tmp_name"], $location);
		 
		 $this->db->set('file_name', $name);
		 $this->db->where('id', 1);
		 $this->db->update('raw_employee_file_storage'); // gives UPDATE `mytable` SET `field` = 'field+1' WHERE `id` = 2
		 echo $name;
		}else{
			exit();
		}
	}

public function add_employee_record(){
	$this->load->database(); 
	if(isset($_POST['category_name']) && isset($_POST['profile_company_name']) && isset($_POST['profile_dept_name'])){
		$category_name=$_POST['category_name'];
		$profile_company_name=$_POST['profile_company_name'];
		$profile_dept_name=$_POST['profile_dept_name'];
		$get_category=$this->db->get_where('raw_employee_file_storage',array('id' => 1 ));
		foreach ($get_category->result() as $row)
	    {		
	    	$employee_photo=$row->file_name;
	      }
		$category = array(
	      	'company_name' => $profile_company_name,
	      	'dept_name' => $profile_dept_name,
	      	'employee_name' => $category_name,
	      	'employee_photo' => $employee_photo
	      );
		$this->db->insert('employee_record', $category);
		echo 'true' ;
		exit();
		
	}
 }

 public function fetch_employee_record(){
	$this->load->database(); 
	if(isset($_POST['profile_company_name_b']) && isset($_POST['profile_dept_name_b'])){
		//$category_name=$_POST['category_name'];
		$profile_company_name_b=$_POST['profile_company_name_b'];
		$profile_dept_name_b=$_POST['profile_dept_name_b'];
		$get_category=$this->db->get_where('employee_record',array('company_name' => $profile_company_name_b, 'dept_name' =>  $profile_dept_name_b));
		foreach ($get_category->result() as $row)
	    {		
	    	
	    	echo '<li class="list-group-item text-bold">'.$row->employee_name.'</li>';
	      }
		
		
		exit();
		
	}
 }

 public function add_department(){
	$this->load->database(); 
	if(isset($_POST['cat_name']) && isset($_POST['sub_category_name'])){
		$cat_name=$_POST['cat_name'];
		$sub_category_name=$_POST['sub_category_name'];
		$get_category=$this->db->get_where('client_business_details',array('company_name' => $cat_name ));
		foreach ($get_category->result() as $row)
	    {		
	    	$sub_category=$row->departments;
	      }
		if($sub_category==''){
			$sub_category_array=$sub_category_name;
		}else{
			$sub_category_array=$sub_category.','.$sub_category_name;
		}
		$this->db->set('departments', $sub_category_array);
		$this->db->where('company_name', $cat_name);
		$this->db->update('client_business_details');
		echo $sub_category_array;
		exit();
		
	}
 }

 public function get_departments_value(){
	$this->load->database(); 
	if(isset($_POST['state_id'])){
		$state_id=$_POST['state_id'];
		$get_category=$this->db->get_where('client_business_details',array('company_name' => $state_id ));
		foreach ($get_category->result() as $row)
	    {		
	    	$sub_category=$row->departments;
	      }
		if($sub_category==''){
			echo 'blank';
			exit();
		}else{
			$sub_array=explode(',', $sub_category);
	    	$size=sizeof($sub_array);
	    	    echo '<option>select</option>';
	    	for($i=0;$i<=$size-1;$i++){
	    		echo '<option value="'.$sub_array[$i].'">'.$sub_array[$i].'</option>';
	    	}
		}
		
		exit();
		
	}
 }

 public function getemployee(){
	$this->load->database(); 
	?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<?php
	if(isset($_POST['profile_company_name_b']) && isset($_POST['profile_dept_name_b'])){
		$profile_company_name_b=$_POST['profile_company_name_b'];
		$profile_dept_name_b=$_POST['profile_dept_name_b'];
		$get_category=$this->db->get_where('employee_record',array('company_name' => $profile_company_name_b, 'dept_name' => $profile_dept_name_b ));
		foreach ($get_category->result() as $row)
	    {		
	    	$employee_name=$row->employee_name;
	    	$employee_photo=$row->employee_photo;
	    	echo '<li class="list-group-item text-bold" style="padding:20px;"><span><img class="profile-user-img img-responsive img-circle" style="hight:100px; width:100px; display:inline; padding-right:40px;"    src="'.base_url('assets/img/client/employees/'.$employee_photo).'"></span><b>'.$employee_name.'</b><button onClick="get_employee_id(this.id);" id="'.$employee_photo.'" class="btn btn-lg btn-secondary pull-right"><i class="fa fa-chevron-right"></i></button></li>';
	      }
		
		}
		
		exit();
	
 }
	

 public function getemployee_re(){
	$this->load->database(); 
	?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<?php
	if(isset($_POST['profile_company_name_b']) && isset($_POST['profile_dept_name_b'])){
		$profile_company_name_b=$_POST['profile_company_name_b'];
		$profile_dept_name_b=$_POST['profile_dept_name_b'];
		$get_category=$this->db->get_where('employee_record',array('company_name' => $profile_company_name_b, 'dept_name' => $profile_dept_name_b ));
		foreach ($get_category->result() as $row)
	    {		
	    	$employee_name=$row->employee_name;
	    	$employee_photo=$row->employee_photo;
	    	echo '<li class="list-group-item text-bold" style="padding:20px;"><span><img class="profile-user-img img-responsive img-circle" style="hight:100px; width:100px; display:inline; padding-right:40px;"    src="'.base_url('assets/img/client/employees/'.$employee_photo).'"></span><b>'.$employee_name.'</b><button onClick="get_employee_id_re(this.id);" id="'.$employee_photo.'" class="btn btn-lg btn-secondary pull-right"><i class="fa fa-chevron-right"></i></button></li>';
	      }
		
		}
		
		exit();
	
 }

public function add_employee_next(){
	$this->load->database(); 
	if(isset($_POST['val'])){
	$val=$_POST['val'];
	$get_category=$this->db->get_where('client_business_details',array('company_name' => $val ));
    foreach ($get_category->result() as $row)
    {	
    	$sub_category=$row->departments;
      }

     if($sub_category==''){
			return 0;
			exit();
		}else{
			return 1;
			exit();
		}
	}
 }	

	

}
