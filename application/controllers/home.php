<?php
header('Access-Control-Allow-Origin: *');
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {	

	function __construct() {
	parent::__construct();

	$this->load->library('pagination');
	$this->load->helper('form');
	$this->load->library('form_validation');
	$this->load->helper('url');
	$this->load->helper('date');
	}
	 
	public function index()
	{
		$this->load->view('home');
	}

	public function client_login()
	{
		$this->load->view('client_login');
	}

	public function client_registration()
	{
		$this->load->view('client_registration');
	}

	public function client_dashboard()
	{
		$this->load->database();
		$this->load->view('client/client_dashboard');
	}

	public function form_automation()
	{
		$this->load->database();
		$this->load->view('client/form_automation');
	}

	public function visitor_management()
	{
		$this->load->database();
		$this->load->view('client/visitors/visitor_management');
	}

	public function automation_launch()
	{
		$this->load->database();
		$this->load->view('client/visitors/automation_launch');
	}

	public function demo_launch()
	{
		
		$this->load->database();
		$this->load->view('client/demo_launch_page');
	}

	public function launch_model_id()
	{
		$this->load->database();
		$query = $this->db->query("SELECT * FROM client_form_automation ORDER BY id DESC LIMIT 1");
        foreach ($query->result() as $row)
        { 
          $model_id=$row->id;
          
          }
          echo $model_id;
          exit();
	}

	public function logout(){
	// Removing session data
	$sess_array = array(
	'username' => '',
	'first_name' => '',
	'last_name' => '',
	'phone_no' => '',
	'email' => ''
	);
	$this->session->unset_userdata('logged_in', $sess_array);
	header("location: client_login");
	}

	public function file_upload_step_2_logo(){
		$this->load->database();
		if($_FILES["select_file"]["name"] != '')
		{
		 $test = explode('.', $_FILES["select_file"]["name"]);
		 $ext = end($test);
		 $name = rand(100000, 999999).'.'.$ext;
		 $location = $_SERVER['DOCUMENT_ROOT'].'/int58/assets/img/client/'.$name;  
		 move_uploaded_file($_FILES["select_file"]["tmp_name"], $location);
		 
		 $this->db->set('logo_file_name', $name);
		 $this->db->where('id', 1);
		 $this->db->update('raw_file_storage'); // gives UPDATE `mytable` SET `field` = 'field+1' WHERE `id` = 2
		 echo $name;
		}else{
			echo 'hui';
		}
	}

	public function file_upload_step_2_theme(){
		$this->load->database();
		if($_FILES["select_file_2"]["name"] != '')
		{
		 $test = explode('.', $_FILES["select_file_2"]["name"]);
		 $ext = end($test);
		 $name = rand(100000, 999999).'.'.$ext;
		 $location = $_SERVER['DOCUMENT_ROOT'].'/int58/assets/img/client/'.$name;  
		 move_uploaded_file($_FILES["select_file_2"]["tmp_name"], $location);
		 
		 $this->db->set('theme_file_name', $name);
		 $this->db->where('id', 1);
		 $this->db->update('raw_file_storage'); // gives UPDATE `mytable` SET `field` = 'field+1' WHERE `id` = 2
		 echo $name;
		}else{
			echo 'hui';
		}
	}

	public function save_automation(){
		$this->load->database();
		if(isset($_POST['form_email']) && isset($_POST['form_dob']) && isset($_POST['form_govt'])  && isset($_POST['auth_level']) && isset($_POST['logo_file_name']) && isset($_POST['auto_name'])){
	    $form_email=$_POST['form_email'];
	    $form_dob=$_POST['form_dob'];
	    $form_govt=$_POST['form_govt'];
	    $auth_level=$_POST['auth_level'];
	    $logo_file_name=$_POST['logo_file_name'];
	    $auto_name=$_POST['auto_name'];

	    $client_id=($this->session->userdata['logged_in']['client_id']);

	        $data = array(
	        'client_id' => $client_id,
	        'form_email' => $form_email,
	        'form_dob' => $form_dob,
	        'govt_id' => $form_govt,
	        'authentication_level' => $auth_level,
	        'logo_file_name' => $logo_file_name,
	        'automation_name' => $auto_name
	        );
	        $this->db->insert('client_form_automation', $data);
	      
	      echo 'true';
	   
	  }
	}
}
