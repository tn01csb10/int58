
<?php
   require 'include/check_session.php';
?>
  <!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Bind Visitors | Add Company</title>
  <?php 
  require_once 'include/header.php'; 
  ?>
</head>
<body class="hold-transition skin-blue-light sidebar-mini">

<div class="wrapper">
<!-- Site wrapper -->
  <?php require_once 'include/navbar.php'; ?>
  <?php require_once 'include/asidebar.php'; ?>
  <!-- Site Carousel -->
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
         Company Listing
        <small>Admin</small>
      </h1>
      
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-6">
          <!-- Default box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add company</h3>
            </div>
            <div class="box-body">
              <div class="form-group">
                <label for="exampleInputEmail1">Company Name</label>
                <input type="email" class="form-control" id="add_company_name" name="add_company_name" placeholder="Enter company name">
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" id="add_company_submit" name="add_company_submit" class="btn btn-primary">Add company</button>
            </div>
          <!-- /.box-footer-->
          </div>
        </div>
        <div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Company list</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                  <i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <?php
                $get_company=$this->db->get('client_business_details');
                foreach ($get_company->result() as $row)
                {
                    echo '<li class="list-group-item text-bold">'.$row->company_name.'</li>';
                  }
              ?>
            </div>
          </div>
        </div>
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-6">
          <!-- Default box -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Add department</h3>
            </div>
            <div class="box-body">
              <div class="form-group">
                <label>Select Company <span style="color: red;"> *</span></label>
                  <select onChange="getlist(this.value);"   class="form-control" id="getlist" >
                  <option value="">Select Company</option>
                  <?php
                     $lead_o = $this->db->query("SELECT * from client_business_details ");
                    foreach ($lead_o->result() as $lead_mails)
                      {  
                          echo '<option value="'.$lead_mails->company_name.'">'.$lead_mails->company_name.'</option>';
                        }
                    ?>
                  </select>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Enter department Name</label>
                <input type="text" class="form-control" id="add_sub_category_name" name="add_sub_category_name" placeholder="Enter department name">
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" id="add_sub_category_submit" name="add_sub_category_submit" class="btn btn-primary">Add department</button>
            </div>
          <!-- /.box-footer-->
          </div>
        </div>
        <div class="col-md-6">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Department list for <strong><span id="cat_name_field"></span></strong></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                  <i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body sub_cat_box">
             
            </div>
          </div>
        </div>
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
 
  

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-6">
          <!-- Default box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add employee host</h3>
            </div>
            <div class="box-body">
              <div class="form-group">
                 <label for="exampleFormControlSelect1">Select company</label>
                    <select onChange="getdistrict(this.value);" name="sort" class="form-control" id="select_company_dropdown">
                      <option>Select</option>
                      <?php
                      $lead_o = $this->db->query("SELECT * from client_business_details ");
                      foreach ($lead_o->result() as $lead_mails)
                        {  
                            echo '<option value="'.$lead_mails->company_name.'">'.$lead_mails->company_name.'</option>';
                          }
                      ?>
                     
                    </select>
              </div>

              <div class="form-group" onChange="getemployee(this.value);" id="department_list_show">
                     <label for="exampleFormControlSelect1">Select</label>
                      <select class="form-control district-list" name="sortb" id="select_department_list">
                       
                        <option>select</option>
                        
                      </select>
                </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Employee name</label>
                <input type="text" class="form-control" id="add_employee_name" name="add_employee_name" placeholder="Enter employee name">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Employee photo</label>
                <input type="file" name="select_file" id="select_file"><span style="" id="file_upload_error"></span>
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" id="add_employee_details" name="add_employee_details" class="btn btn-primary">Add employee</button>
            </div>
          <!-- /.box-footer-->
          </div>
        </div>
        <div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title" >Employee list</h3>
              
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                  <i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <center><p id="employee_head"></p></center>
              <select class="form-control district-list" >
                       
                        
                      </select>
              <script type="text/javascript">
                $(document).on('change', '#select_department_list', function(){
                  var profile_company_name_b=$('#select_company_dropdown').val();
                  var profile_dept_name_b=$('#select_department_list').val();
                  $.ajax({  
                    url:"<?php echo base_url();?>client/dashboard/fetch_employee_record",  
                    method:"POST",  
                    data:{profile_company_name_b:profile_company_name_b, profile_dept_name_b:profile_dept_name_b},
                    beforeSend:function(){
                      $('#employee_head').html('Loading...').after("&nbsp;<img class='spc_img' src='<?php echo base_url()?>assets/img/spinner.GIF' >").fadeIn(); 
                    },  
                    success:function(data){
                      $('#employee_head').nextAll().remove();
                      $('#employee_head').html('Employee list');
                     
                    },
                    error: function (data) {// When Service call fails
                    }  
                  });
                });

              </script>
              
            </div>
          </div>
        </div>
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
<script type="text/javascript">
$('#add_employee_details').on('click',function(){
      if($('#add_employee_name').val()== ""){
        $('#add_employee_name').focus();
        $('#add_employee_name').css('border','0.8px solid red');
      }else{
        var category_name=$('#add_employee_name').val();
        var profile_company_name=$('#select_company_dropdown').val();
        var profile_dept_name=$('#select_department_list').val();
        $.ajax({  
          url:"<?php echo base_url();?>client/dashboard/add_employee_record",  
          method:"POST",  
          data:{category_name:category_name, profile_company_name:profile_company_name, profile_dept_name:profile_dept_name},
          beforeSend:function(){
            $('#add_employee_details').html('Please wait...').after("&nbsp;<img class='spc_img' src='<?php echo base_url()?>assets/img/spinner.GIF' >").fadeIn(); 
          },  
          success:function(data){
            $('#add_employee_details').nextAll().remove();
            $('#add_employee_details').html('Employee added successfully');
           
          },
          error: function (data) {// When Service call fails
          }  
        });  
      }
    })


 $(document).on('change', '#select_file', function(){
  var name = document.getElementById("select_file").files[0].name;
  
  var form_data = new FormData();
  var ext = name.split('.').pop().toLowerCase();
  if(jQuery.inArray(ext, ['png','jpg','jpeg']) == -1) 
  {
   //alert("Invalid Image File");
   $('#select_file').val("");
   $('#select_file').css('border','0.8px solid red');
   $('#file_upload_error').html("<label class='text-danger'>Invalid File type</label>");
   //$('#file_upload_preview').html('');
  }
   else{
    form_data.append("select_file", document.getElementById('select_file').files[0]);
     $.ajax({
      url:"<?php echo base_url('client/dashboard/employee_data');?>",
      method:"POST",
      data: form_data,
      contentType: false,
      cache: false,
      processData: false, 
      beforeSend:function(){
      // $('#file_upload_error').html("<label class='text-success'>File Uploading...</label>");
      },   
      success:function(data)
      {
      $('#select_file').css('border','0.8px solid green');
      $('#file_upload_error').html("<label class='text-success'>File Uploaded successfully.</label>");
      //$('#file_upload_preview').html('<embed src="<?php //echo base_url()?>uploaded_files/'+data+'" height="150" width="225"  />');
      $('#file_upload_preview').html('<div style="height:200px" class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item img-responsive"  src="<?php echo base_url()?>assets/img/client/'+data+'"></iframe></div>');
      
      }
     });
   }
  
 });



  function getdistrict(val) {
    $.ajax({
    type: "POST",
    url: "<?php echo base_url('client/dashboard/get_departments_value')?>",
    data:'state_id='+val,
    success: function(data){
      if(data == 'blank'){
        $('#department_list_show').hide();
      }else{
        $('#department_list_show').show();
        $(".district-list").html(data);
      }
    }
    });
  }

   function getemployee(val) {
    $.ajax({
    type: "POST",
    url: "<?php echo base_url('client/dashboard/getemployee')?>",
    data:'department_id='+val,
    success: function(data){
      if(data == 'blank'){
        $('#department_list_show').hide();
      }else{
        $('#department_list_show').show();
        $(".district-list").html(data);
      }
    }
    });
  }
</script>

 <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  </div>
</div>
<?php
require_once 'include/footer.php';
?>
<script type="text/javascript">



  $('#add_employee_next').on('click',function(){
      if($('#add_company_name').val()== ""){
        $('#add_company_name').focus();
        $('#add_company_name').css('border','0.8px solid red');
      }else{
        var category_name=$('#add_company_name').val();
        $.ajax({  
          url:"<?php echo base_url();?>client/dashboard/add_employee_next",  
          method:"POST",  
          data:{category_name:category_name},
          beforeSend:function(){
            $('#add_employee_next').html('Please wait...').after("&nbsp;<img class='spc_img' src='<?php echo base_url()?>assets/img/spinner.GIF' >").fadeIn(); 
          },  
          success:function(data){
            $('#add_employee_next').nextAll().remove();
            $('#add_employee_next').html('Add category');
            location.href="<?php echo base_url()?>client/dashboard/add_company";
          },
          error: function (data) {// When Service call fails
          }  
        });  
      }
    })

  $('#add_company_submit').on('click',function(){
      if($('#add_company_name').val()== ""){
        $('#add_company_name').focus();
        $('#add_company_name').css('border','0.8px solid red');
      }else{
        var category_name=$('#add_company_name').val();
        $.ajax({  
          url:"<?php echo base_url();?>client/dashboard/add_company_name",  
          method:"POST",  
          data:{category_name:category_name},
          beforeSend:function(){
            $('#add_company_submit').html('Creating please wait...').after("&nbsp;<img class='spc_img' src='<?php echo base_url()?>assets/img/spinner.GIF' >").fadeIn(); 
          },  
          success:function(data){
            $('#add_company_submit').nextAll().remove();
            $('#add_company_submit').html('Add category');
            location.href="<?php echo base_url()?>client/dashboard/add_company";
          },
          error: function (data) {// When Service call fails
          }  
        });  
      }
    })

    $('#add_sub_category_submit').on('click',function(){
      if($('#add_sub_category_name').val()== ""){
        $('#add_sub_category_name').focus();
        $('#add_sub_category_name').css('border','0.8px solid red');
      }else{
        var cat_name=$('#getlist').val();
        var sub_category_name=$('#add_sub_category_name').val();
        $.ajax({  
          url:"<?php echo base_url();?>client/dashboard/add_department",  
          method:"POST",  
          data:{cat_name:cat_name, sub_category_name:sub_category_name},
          beforeSend:function(){
            $('#add_sub_category_submit').html('Creating please wait...').after("&nbsp;<img class='spc_img' src='<?php echo base_url()?>assets/img/spinner.GIF' >").fadeIn(); 
          },  
          success:function(data){
            $('#add_sub_category_submit').nextAll().remove();
            $('#add_sub_category_submit').html('Add department');
            location.href="<?php echo base_url()?>client/Dashboard/add_company";
          },
          error: function (data) {// When Service call fails
          }  
        });  
      }
    })

    function getlist(val) {
      $('#cat_name_field').html(val);
      $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>client/dashboard/sub_category_info",
      data:{val:val},
      beforeSend:function(){
        $('.sub_cat_box').html("<br><center><img class='spc_img' src='<?php echo base_url()?>assets/img/spinner.GIF' ></center>");
      }, 
      success: function(data){
        $(".sub_cat_box").html(data);
      }
      })
    };

 //multiselect ..
 $('#framework').multiselect({
    nonSelectedText: 'Select Category',
    enableFiltering: true,
    enableCaseInsensitiveFiltering: true,
    buttonWidth:'400px'
 });
 
 $('#framework_form').on('submit', function(event){
     event.preventDefault();
     var form_data = $(this).serialize();
     $.ajax({
     url:"insert.php",
     method:"POST",
     data:form_data,
     success:function(data)
     {
      $('#framework option:selected').each(function(){
       $(this).prop('selected', false);
      });
      $('#framework').multiselect('refresh');
      alert(data);
     }
  });
 });

</script>
</body>
</html>

