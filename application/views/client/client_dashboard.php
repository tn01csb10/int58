
<?php
   require 'include/check_session.php';
?>
  <!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Bind Visitors | Client Dashboard</title>
  <?php 
  require_once 'include/header.php'; 
  ?>
</head>
<body class="hold-transition skin-blue-light sidebar-mini">

<div class="wrapper">
<!-- Site wrapper -->
  <?php require_once 'include/navbar.php'; ?>
  <?php require_once 'include/asidebar.php'; ?>
  <!-- Site Carousel -->
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <div  style="margin-top: 0;">
      <?php
      $query = $this->db->query('SELECT * FROM client_form_automation');
      $numq= $query->num_rows();
      if($numq > 0){
        ?>
        <div class="container" style="padding: 20px;">
          <div class="row">
            <h4 class="lead">Launch Automation</h4>
            <?php 
            foreach ($query->result() as $row)
            { 
              $auto_name=$row->automation_name;
              $auto_id=$row->id;
              ?>
              <script type="text/javascript"></script>
            <div class="col-md-3">

              <!-- Profile Image -->
              <div class="box box-primary">
                <div class="box-body box-profile">

                  <p class="text-muted text-center"><b><?php echo $auto_name?></b></p>

                  <a href="#" id="<?php echo $auto_id?>" onClick="launch_auto(this.id)" class="btn btn-primary btn-block"><b>Launch now</b></a>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
            </div>
            <?php
            }
            ?>

          </div>
          <div class="row" style="padding: 20px;">
            <div class="col-md-12" style="background: #ECF0F5; padding: 30px;">
              <center><a  href="<?php echo base_url('home/form_automation'); ?>" style="border:dashed #B3B5BB;background: #F8F9FA;color: #000;" class="btn btn-md btn-secondary text-bold ">+  Create Visitor Form Now</a></center>
            </div>
          </div>
        </div>
         
      <?php
      }else{
        ?>
        <div class="container">
            <div class="row">
        <div class="col-md-12">
          <h1>Client Dashboard</h1>
        </div>
        </div><br><br>
        <div class="row" style="background: #EEF3F9; padding:60px;">
        <div class="col-md-12">
          <center><img src="<?php echo base_url();?>assets/img/landing-page.SVG" style="width: 150px; height: auto;" >
          <h2 class="lead text-bold">You haven't created any Visitor form yet</h2>
          <h5 class=" text-secondary">Automate visitor registration and manage them using automation tools!</h5><br>
          <a  href="<?php echo base_url('home/form_automation'); ?>" style="border:dashed #B3B5BB;background: #F8F9FA;color: #000;" class="btn btn-md btn-secondary text-bold ">+  Create Visitor Form Now</a>
          </center>
        </div>
       </div>
      </div>
        <?php
      }
      ?>
      

    </div>
  </div>
</div>
<?php
require_once 'include/footer.php';
?>
<script type="text/javascript">
   function launch_auto(val){
   // alert(val);
    var value_modifier= val*48;
    window.location.href="<?php echo base_url('home/demo_launch/?demo=')?>"+value_modifier;
   }

</script>
</body>
</html>

