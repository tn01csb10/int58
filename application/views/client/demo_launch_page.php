
<?php
   require 'include/check_session.php';
   if(isset($_GET['demo'])){
    $demo=$_GET['demo'];
    $unwind_id=$demo/48;
    $get_page_details=$this->db->get_where('client_form_automation',array('id' => $unwind_id ));
      foreach ($get_page_details->result() as $row_data)
      { 
        $form_data_client_id=$row_data->client_id;
        $form_data_form_email=$row_data->form_email;
        $form_data_form_dob=$row_data->form_dob;
        $form_data_form_govt=$row_data->govt_id;
        $form_data_authentication_level=$row_data->authentication_level;
        $form_data_logo_file_name=$row_data->logo_file_name;
      }
    }
?>
  <!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Bind Visitors | Demo Launch</title>
  <?php 
  require_once 'include/header.php'; 
  ?>

  <style type="text/css">

  body, html {

     height: 100%;
    background: url(<?php echo base_url('assets/img/launch_form_bg2.jpg')?>) no-repeat center center fixed; 
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    opacity: 0.8;
   
   
  } 

    .form_layout{
      text-align: center;
      
    }

    .form_style{
      padding: 10px;
    }

    .form-control{
      padding: 20px; 
      border-radius: 5px; 
      font-size: 18px;
      background: #fff;
    }

    .dropdown-form-control{
      padding: 5px; 
      border-radius: 5px; 
      font-size: 18px;
      background: #fff;
    }

    .form-controll{
       padding: 5px; 
      border-radius: 5px; 
      font-size: 18px;
      background: white;
    }

    .step_1_btn{
      background: transparent;
      border: 3px solid white; 
      border-radius: 5px; 
      font-size: 18px;
      padding: 20px;
      color: white;
          }

    .form_paging{
      position: absolute;
      left: 50%;
      top:50%;
      transform:translate(-50%,-50%);
      border-radius: 10px;
    }

    .head_style{
      color: #fff;
    }
  </style>
</head>
<body class="hold-transition skin-blue-light" >
<!-- Site wrapper -->
  
<!-- Step 1 -->
<div class="container form_paging" id="step_1" >
  <div class="row">
    <div class="col-md-12" style="padding: 100px;">
      <div class="form_layou ">
        <div class="form-group" class="form_style">
            <button class="btn btn-lg btn-block step_1_btn" id="step_1_first_time">First time user here </button>
         </div>
         <div class="form-group" class="form_style">
            <button class="btn btn-lg btn-block step_1_btn " id="step_1_returning">Returning user here</button>
         </div>
         <div class="form-group" class="form_style">
            <button class="btn btn-lg btn-block step_1_btn ">I have an invitation</button>
         </div>
      </div>
    </div>
  </div>
</div>

<!-- Step 2 returning user-->
<div class="container form_paging" id="step_2_re" >
  <div class="row">

    <div class="col-md-12" style="padding: 100px; background:#E5E5E5; border: solid transparent; border-radius: 5px; ">
      <div class="form_layout ">
        <div class="form-group" class="form_style">
            <input type="email" required="" class="form-control" id="visitor_phone_check" placeholder="Enter your email id">
         </div><br>
         <div class="form-group" class="form_style">
            <button class="btn btn-lg btn-block btn-success pull-right" id="step_2_re_next">Get OTP</button>
         </div>
      </div>
    </div>
  </div>
</div>

<!-- Step 2 -->
<div class="container form_paging" id="step_2">
  
 <div class="row">
    <center><button id="step_1_first_time_back" class="btn btn-md pull-left"><i class="fa fa-chevron-left"></i></button><h1 class="head_style" style="display: inline;">Your details please</h1><button class="btn btn-lg btn-success pull-right" id="step_3_first_time_next">Next</button></center>
    <div class="col-md-12" style="padding-top: 40px;">
      <div class ="form_layout">
        
        <div class="form-group" >
            <input type="text"  class="form-control" id="f_name" placeholder="First name">
        </div>
        <div class="form-group" >
            <input type="text"  class="form-control" id="l_name" placeholder="Last name">
        </div>
    
        <div class="form-group">
            <input type="number" class="form-control" id="visitor_phone" placeholder="Mobile">
        </div>
        <?php
          if ($form_data_form_email == "Set"){
            ?>
            <div class="form-group">
                <input type="email" class="form-control" id="visitor_email" required="" placeholder="Email">
            </div>
            <?php
          }else{
            ?>
            <div class="form-group">
                <input type="email" class="form-control" id="visitor_email" placeholder="Email(Optional)">
            </div>
            <?php
          }

          if ($form_data_form_dob == "Set"){
            ?>
            <div class="form-group">
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker" placeholder="Dtae of birth" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                </div>
            </div>
            <?php
          }else{
            ?>
            <div class="form-group">
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker" placeholder="Dtae of birth(Optional)" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                </div>
            </div>
            <?php
          } 

          if ($form_data_form_govt == "Set"){
            ?>
            <div class="form-group" >
              <select onChange="get_id_type(this.value);" class="form-control dropdown-form-control" id="govt_card"  >
                <option selected="selected" value="">Select ID type</option>
                <option value="Aadhar Card">Aadhar Card</option>
                <option value="PAN Card">PAN Card</option>
                <option value="Voter ID Card">Voter ID Card</option>
                <option value="Driving licence">Driving licence</option>
                <option value="NREGA Card">NREGA Card</option>
                <option value="Passport">Passport</option>
                </select>
            </div>

            <div class="form-group" id="hidden_id_taker" style="display: none;">
              <input type="text" class="form-control" id="visitor_id_number" placeholder="Enter ID Number">
            </div>
            <?php
          }else{
            ?>
            <div class="form-group">
              <select onChange="get_id_type(this.value);" class="form-control dropdown-form-control"   id="govt_card" >
                <option selected="selected" value="">Select ID type</option>
                <option value="Aadhar Card">Aadhar Card</option>
                <option value="PAN Card">PAN Card</option>
                <option value="Voter ID Card">Voter ID Card</option>
                <option value="Driving licence">Driving licence</option>
                <option value="NREGA Card">NREGA Card</option>
                <option value="Passport">Passport</option>
                </select>
            </div>

            <div class="form-group" id="hidden_id_taker" style="display: none;">
              <input type="text" class="form-control" id="visitor_id_number" placeholder="Enter ID Number(Optional)">
            </div>
            <?php
          } 
        ?>
        
        <div class="dropdown">
          <div class="form-group" >
            <select  class="form-control dropdown-form-control" id="reason" >
              <option selected="selected" value="">Reason to visit</option>
              <option value="Bussiness">Bussiness</option>
              <option value="Personal">Personal</option>
              <option value="Courier Delivery">Courier Delivery</option>
              <option value="Other">Other</option>
              </select>
          </div>
      </div>
    </div>
  </div>
</div>
</div>

<!-- Modal Verification -->
<div style=" position: absolute;
      left: 50%;
      top:50%;
      transform:translate(-50%,-50%);
      border-radius: 10px;" class="modal fade" id="otp_ver_tab_level_1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Complete verification</h4>
      </div>
      
      <div class="modal-body" style="padding: 30px;">
        <div class="alert alert-danger alert-dismissible " id="sent_failed" role="alert" style="display: none;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        You have intered incorrect OTP <strong> try again!</strong>
        </div>
        <div class="form-group">
          <center><input type="number" style="width: 50%" name="" id="type_otp" class="form-control" placeholder="Enter OTP">
          </center>
        </div>
        <div class="form-group">
          <center><button type="button" id="submit_otp" class="btn  btn-success" style="padding: 5px 30px;">SUBMIT</button></center>
        </div>
        <div class="form-group">
          <center><a id="resend_otp" style="" class="">Resend OTP</a></center>
        </div>
        
      </div>
      <div class="modal-footer">
       
      </div>
    </div>
  </div>
</div>

<!-- Modal Verification -->
<div style=" position: absolute;
      left: 50%;
      top:50%;
      transform:translate(-50%,-50%);
      border-radius: 10px;" class="modal fade" id="otp_ver_tab_re" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Complete verification</h4>
      </div>
      
      <div class="modal-body" style="padding: 30px;">
        <div class="alert alert-danger alert-dismissible " id="sent_failed_re" role="alert" style="display: none;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        You have intered incorrect OTP <strong> try again!</strong>
        </div>
        <div class="alert alert-danger alert-dismissible " id="verify_failed_re" role="alert" style="display: none;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        This number is not register with us, kindly check your number and <strong> try again!</strong>
        </div>
        <div class="form-group">
          <center><input type="number" style="width: 50%" name="" id="type_otp_re" class="form-control" placeholder="Enter OTP">
          </center>
        </div>
        <div class="form-group">
          <center><button type="button" id="submit_otp_re" class="btn  btn-success" style="padding: 5px 30px;">SUBMIT</button></center>
        </div>
        <div class="form-group">
          <center><a id="resend_otp_re" style="" class="">Resend OTP</a></center>
        </div>
        
      </div>
      <div class="modal-footer">
       
      </div>
    </div>
  </div>
</div>

<!-- Modal Verification -->
<div style=" position: absolute;
      left: 50%;
      top:50%;
      transform:translate(-50%,-50%);
      border-radius: 10px;" class="modal fade" id="otp_ver_tab_level_2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Complete verification</h4>
      </div>
      
      <div class="modal-body" style="padding: 30px;">
        <div class="alert alert-danger alert-dismissible " id="sent_failed_mail" role="alert" style="display: none;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        You have intered incorrect OTP <strong> try again!</strong>
        </div>
        <div class="alert alert-success alert-dismissible " id="sent_success_mail" role="alert" style="display: none;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        OTP has been sent to your mail id <strong> successfully!</strong>
        </div>
        <div class="form-group">
          <center><input type="number" style="width: 50%" name="" id="type_otp_2" class="form-control" placeholder="Enter OTP you recieve on phone">&nbsp;<a id="resend_otp" style="" class="">Resend OTP SMS</a>
          </center>
        </div>
        <div class="form-group">
          <center><input type="number" style="width: 50%" name="" id="type_otp_mail" class="form-control" placeholder="Enter OTP you recieve on email"><a id="resend_otp_2" style="" class="">Resend OTP email</a>
          </center>
        </div>
        <div class="form-group">
          <center><button type="button" id="submit_otp_2" class="btn  btn-success" style="padding: 5px 30px;">SUBMIT</button></center>
        </div>
        <div class="form-group">
          <center></center>
        </div>
        <div class="form-group">
          <center></center>
        </div>
        
      </div>
      <div class="modal-footer">
       
      </div>
    </div>
  </div>
</div>


<!-- Modal checkin -->
<div style=" position: absolute;
      left: 50%;
      top:50%;
      transform:translate(-50%,-50%);
      border-radius: 10px;" class="modal fade" id="otp_check_in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
      </div>
      
      <div class="modal-body" style="padding: 30px;">
        <div class="alert alert-danger alert-dismissible " id="sent_failed_check" role="alert" style="display: none;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        Couldn't check in you at a moment <strong>please try again later!</strong>
        </div>
        <div class="form-group">
          <center><h4 class="modal-title" id="myModalLabel">Checking you in</h4>
            <p class="lead">please wait one moment</p>
          </center>
        </div>
        
      </div>
      <div class="modal-footer">
       
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
 
</script>

<!-- Step 4 -->
<div class="container " style="padding-top: 50px;" id="step_4">
  <div class="row">
    <center><button id="step_3_first_time_back" class="btn btn-md pull-left" ><i class="fa fa-chevron-left"></i></button><h1 class="head_style" style="display: inline;">Who are you seeing?</h1></center>
    
    <div class="col-md-6" style="padding-top: 40px;">
      
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Select company</h3>
        </div>
        <div class="box-body">
        <select onChange="getdistrict(this.value);" name="sort" class="form-control form-controll" id="select_company_dropdown">
          <option>Select</option>
          <?php
          $lead_o = $this->db->query("SELECT * from client_business_details ");
          foreach ($lead_o->result() as $lead_mails)
            {  
                echo '<option value="'.$lead_mails->company_name.'">'.$lead_mails->company_name.'</option>';
              }
          ?>
         
        </select>

        <div class="form-group" onChange="getemployee(this.value);" id="department_list_show">
             <label for="exampleFormControlSelect1">Select department</label>
              <select class="form-control district-list form-controll" name="sortb" id="select_department_list">
               
                <option>select</option>
                
              </select>
        </div>
    <!-- /.box-body -->
        
      <!-- /.box-footer-->
      </div>
    </div>
    </div>
    <div class="col-md-6" style="padding-top: 40px;">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title text-bold">List of employees</h3>
        </div>
       <div class="box-body employee-list">
          
        </div>
      </div>
    </div>
  </div>
</div>



<!-- Step 4 -->
<div class="container " style="padding-top: 50px;" id="step_4_re">
  <div class="row">
    <center><h1 class="head_style" style="display: inline;">Who are you seeing?</h1></center>
    
    <div class="col-md-6" style="padding-top: 40px;">
      
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Select company</h3>
        </div>
        <div class="box-body">
        <select onChange="getdistrict(this.value);" name="sort" class="form-control form-controll" id="select_company_dropdown_re">
          <option>Select</option>
          <?php
          $lead_o = $this->db->query("SELECT * from client_business_details ");
          foreach ($lead_o->result() as $lead_mails)
            {  
                echo '<option value="'.$lead_mails->company_name.'">'.$lead_mails->company_name.'</option>';
              }
          ?>
         
        </select>

        <div class="form-group" onChange="getemployee_re(this.value);" id="department_list_show">
             <label for="exampleFormControlSelect1">Select department</label>
              <select class="form-control district-list form-controll" name="sortb" id="select_department_list_re">
               
                <option>select</option>
                
              </select>
        </div>
    <!-- /.box-body -->
        
      <!-- /.box-footer-->
      </div>
    </div>
    </div>
    <div class="col-md-6" style="padding-top: 40px;">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title text-bold">List of employees</h3>
        </div>
       <div class="box-body employee-list">
          
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Step 5 -->
<div class="container form_paging" id="step_5">
  <div class="row">
    <h1 class="head_style" style="font-size: 40px;">Thank You <span style="font-weight: bolder;" id="visitor_hidden"></span></h1>
   
    <h1 class="head_style" style="font-size: 40px;">You are now checked-in </h1>
   
    <br>
    <h1 class="head_style" style="font-size: 30px;font-style: italic;">Your host has been notified of your arrival. </h1>
    <div class="col-md-12" style="padding: 40px;">
      <div class="form_layout">
        <div class="form-group" class="form_style">
            <button class="btn btn-lg step_1_btn pull-left" id="step_5_first_time_back">Next Visitor</button>
         </div>
        
      </div>
    </div>
  </div>
</div>

<?php
require_once 'include/footer.php';
?>
<script type="text/javascript">
    $(function () {
      //Money Euro
    $('[data-mask]').inputmask()
    })
    //Date picker
    

    $('#one_time_area').on('click',function(){
     $('#send_im').prop('checked', true);
     $('#send_later').prop('checked', false);
     $('#schedule_mail').hide();
    });

     $('#schedule_area').on('click',function(){
       $('#send_later').prop('checked', true);
       $('#send_im').prop('checked', false);
       $('#schedule_mail').show();
    });
  //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    
    function get_id_type(val) {
    if(val == "Aadhar Card"){
      $('#hidden_id_taker').show();
    }else if(val == "PAN Card"){
      $('#hidden_id_taker').show();
    }else if(val == "Voter ID Card"){
      $('#hidden_id_taker').show();
    }else if(val == "Driving licence"){
      $('#hidden_id_taker').show();
    }else if(val == "NREGA Card"){
      $('#hidden_id_taker').show();
    }else if(val == "Passport"){
      $('#hidden_id_taker').show();
    }else{
      $('#hidden_id_taker').hide();
    }
  }

    function getdistrict(val) {
    $.ajax({
    type: "POST",
    url: "<?php echo base_url('client/dashboard/get_departments_value')?>",
    data:'state_id='+val,
    success: function(data){
      if(data == 'blank'){
        $('#department_list_show').hide();
      }else{
        $('#department_list_show').show();
        $(".district-list").html(data);
      }
    }
    });
  }

   function getemployee(val){
    var profile_company_name_b=$('#select_company_dropdown').val();
    var profile_dept_name_b=$('#select_department_list').val();
    $.ajax({
    type: "POST",
    url: "<?php echo base_url('client/dashboard/getemployee')?>",
    data:{profile_company_name_b:profile_company_name_b, profile_dept_name_b:profile_dept_name_b},
    success: function(data){
      if(data == 'blank'){
        //$('#department_list_show').hide();
      }else{
        //$('#department_list_show').show();
        $(".employee-list").html(data);
      }
    }
    });
  }

  function getemployee_re(val){
    var profile_company_name_b=$('#select_company_dropdown_re').val();
    var profile_dept_name_b=$('#select_department_list_re').val();
    $.ajax({
    type: "POST",
    url: "<?php echo base_url('client/dashboard/getemployee_re')?>",
    data:{profile_company_name_b:profile_company_name_b, profile_dept_name_b:profile_dept_name_b},
    success: function(data){
      if(data == 'blank'){
        //$('#department_list_show').hide();
      }else{
        //$('#department_list_show').show();
        $(".employee-list").html(data);
      }
    }
    });
  }

  $('.spcific').on('click',function() {
  //Here this refers to the clicked element so
  alert(this.id)
});

  function get_employee_id(val){
      //id = $(this).attr('id');
     //alert(val);
    // alert($(this).prop('href'));

      var f_name=$('#f_name').val();
      var l_name=$('#l_name').val();
      var datepicker=$('#datepicker').val();
      var visitor_email=$('#visitor_email').val();
      var visitor_phone=$('#visitor_phone').val();
      var hidden_id_taker=$('#visitor_id_number').val();
      var govt_card=$('#govt_card').val();
      var reason=$('#reason').val();
      var unwind_id= <?php echo $unwind_id ?>;
      var employee_meet= val;
      var profile_company_name_b=$('#select_company_dropdown').val();
      var profile_dept_name_b=$('#select_department_list').val();
      var form_data_client_id= <?php echo $form_data_client_id ?>;

      $.ajax({
      
      url:"<?php echo base_url('client/dashboard/insert_visitor_record');?>",
      method:"POST",
      data:{f_name:f_name, l_name:l_name, datepicker:datepicker, visitor_email:visitor_email, visitor_phone:visitor_phone , hidden_id_taker:hidden_id_taker , govt_card:govt_card , reason:reason , unwind_id:unwind_id , employee_meet:employee_meet , profile_company_name_b:profile_company_name_b , profile_dept_name_b:profile_dept_name_b , form_data_client_id:form_data_client_id},
      beforeSend:function(){
        /*$('#otp_ver_tab_level_1').on('hidden.bs.modal', function () {
          // Load up a new modal...
          $('#otp_check_in').modal('show')
        })*/
      // $('#otp_check_in').modal('show');

      }, 
      success: function(data){
        //alert(data);
        if(data == 'true'){
          var f_name=$('#f_name').val();
          var l_name=$('#l_name').val();
          $('#visitor_hidden').html(f_name+' '+l_name);
          $('#step_1').hide();
         $('#step_2').hide();
         $('#step_3').hide();
         $('#step_4').hide();
         $('#step_4_re').hide();
         $('#step_5').show();
         $('#step_2_re').hide();
          /*$('#otp_ver_tab_level_1').on('hidden.bs.modal', function () {
          // Load up a new modal...
          $('#otp_check_in').modal('show')
        })*/
          //$('#otp_check_in').modal('show');
          //alert(data);

        }else{
         // $('#sent_failed_check').css('display','block');
         // alert('error');
        }
      }
      });

    }


  function get_employee_id_re(val){
      //id = $(this).attr('id');
     //alert(val);
    // alert($(this).prop('href'));

      var visitor_email=visitor_number;
      var unwind_id= <?php echo $unwind_id ?>;
      var employee_meet= val;
      var profile_company_name_b=$('#select_company_dropdown_re').val();
      var profile_dept_name_b=$('#select_department_list_re').val();
      var form_data_client_id= <?php echo $form_data_client_id ?>;

      $.ajax({
      
      url:"<?php echo base_url('client/dashboard/insert_visitor_record_re');?>",
      method:"POST",
      data:{visitor_email:visitor_email, unwind_id:unwind_id , employee_meet:employee_meet , profile_company_name_b:profile_company_name_b , profile_dept_name_b:profile_dept_name_b , form_data_client_id:form_data_client_id},
      beforeSend:function(){
        

      }, 
      success: function(data){
        //alert(data);
        
          var name=data;

          
          $('#visitor_hidden').html(name);
          $('#step_1').hide();
         $('#step_2').hide();
         $('#step_3').hide();
         $('#step_4').hide();
         $('#step_4_re').hide();
         $('#step_5').show();
         $('#step_2_re').hide();
         
      }
      });

    }

    $('document').ready(function(){
     $('#step_1').show();
     $('#step_2').hide();
     $('#step_3').hide();
     $('#step_4').hide();
     $('#step_4_re').hide();
     $('#step_5').hide();
     $('#step_2_re').hide();
    }); 

    $('#step_1_returning').on('click',function(){
       $('#step_1').hide();
       $('#step_3').hide();
       $('#step_4').hide();
       $('#step_4_re').hide();
       $('#step_5').hide();
       $('#step_2').hide();
       $('#step_2_re').show();
       $('html,body').scrollTop(0);

   });

    $('#step_1_first_time').on('click',function(){
       $('#step_1').hide();
       $('#step_3').hide();
       $('#step_4').hide();
       $('#step_4_re').hide();
       $('#step_5').hide();
       $('#step_2_re').hide();
       $('#step_2').show();

       $('html,body').scrollTop(0);

   });

    $('#step_2_first_time_next').on('click',function(){
       $('#step_1').hide();
       $('#step_2').hide();
       $('#step_4').hide();
       $('#step_4_re').hide();
       $('#step_5').hide();
       $('#step_2_re').hide();
       $('#step_3').show();
       $('html,body').scrollTop(0);

   });

    $('#step_1_first_time_back').on('click',function(){
       $('#step_2').hide();
       $('#step_3').hide();
       $('#step_4').hide();
       $('#step_4_re').hide();
       $('#step_5').hide();
       $('#step_2_re').hide();
       $('#step_1').show();
       $('html,body').scrollTop(0);

   });

   $('#step_3_first_time_back').on('click',function(){
       $('#step_1').hide();
       $('#step_3').hide();
       $('#step_4').hide();
       $('#step_4_re').hide();
       $('#step_5').hide();
       $('#step_2_re').hide();
       $('#step_2').show();
       $('html,body').scrollTop(0);

   });

   $('#step_2_re_next').on('click',function(){
       var visitor_email= $('#visitor_phone_check').val();
       //alert(visitor_email);
       if(visitor_email == ""){
        $('#visitor_phone_check').css('border','0.8px solid red');
       }else{
        $('#visitor_phone_check').css('border','0.8px solid transparent');
        $.ajax({
          type: "POST",
          url: "<?php echo base_url('client/dashboard/send_mail_otp')?>",
          data:{visitor_email:visitor_email},
          success: function(data){
            //alert(data);
            if(data == 'no'){
              //$('#department_list_show').hide();
            }else{
               sms_otp= data;
              //alert(mail_otp);
              $('#otp_ver_tab_re').modal('show');
            }
          }
          });

       }
       
   });


  $('#step_5_first_time_back').on('click',function(){
       location.reload();
   });

  $('#step_3_first_time_next').on('click',function(){
    var form_data_authentication_level = <?php echo $form_data_authentication_level;?>;
    if(form_data_authentication_level == 2){
      var visitor_number= $('#visitor_phone').val();
      var visitor_email= $('#visitor_email').val();
      $.ajax({
      type: "POST",
      url: "<?php echo base_url('client/dashboard/send_sms_otp')?>",
      data:{visitor_number:visitor_number},
      success: function(data){
        if(data == 'no'){
          //$('#department_list_show').hide();
        }else{
           otp= data;
           alert(otp);
       $.ajax({
          type: "POST",
          url: "<?php echo base_url('client/dashboard/send_mail_otp')?>",
          data:{visitor_email:visitor_email},
          success: function(data){
            
            if(data == 'no'){
              //$('#department_list_show').hide();
            }else{
               mail_otp= data;
              alert(mail_otp);
              $('#otp_ver_tab_level_2').modal('show');
            }
          }
          });
        }
      }
      });

      
    }else if(form_data_authentication_level == 1){
      var visitor_number= $('#visitor_phone').val();
      $.ajax({
      type: "POST",
      url: "<?php echo base_url('client/dashboard/send_sms_otp')?>",
      data:{visitor_number:visitor_number},
      success: function(data){
        if(data == 'no'){
          //$('#department_list_show').hide();
        }else{
           otp= data;
          //alert(otp);
          $('#otp_ver_tab_level_1').modal('show');
        }
      }
      });
      
    }else{
      $('#step_1').hide();
       $('#step_2').hide();
       $('#step_3').hide();
       $('#step_5').hide();
       $('#step_2_re').hide();
       $('#step_4_re').hide();
       $('#step_4').show();
       $('html,body').scrollTop(0);
    }
       

   });

   $('#submit_otp').on('click',function(){
     var get_otp= $('#type_otp').val();
     //alert(get_otp);
     //alert(otp);
     if(get_otp == otp){

      $('#sent_failed').css('display','none');
      $('#otp_ver_tab_level_1').modal('hide');
      $('#step_1').hide();
       $('#step_2').hide();
       $('#step_3').hide();
       $('#step_5').hide();
       $('#step_4_re').hide();
       $('#step_2_re').hide();
       $('#step_4').show();
       $('html,body').scrollTop(0);
   
     }else{
      $('#sent_failed').css('display','block');
     }
    });

   $('#submit_otp_re').on('click',function(){
     var get_otp= $('#type_otp_re').val();
      visitor_number= $('#visitor_phone_check').val();
     var client_id= <?php echo ($this->session->userdata['logged_in']['client_id'])?>;
     //alert(get_otp);
     //alert(otp);
     if(get_otp == sms_otp){

      $.ajax({
      type: "POST",
      url: "<?php echo base_url('client/dashboard/number_check')?>",
      data:{visitor_number:visitor_number, client_id:client_id},
      success: function(data){
        if(data == 'yes'){
          $('#sent_failed_re').css('display','none');
          $('#verify_failed_re').css('display','none');
            $('#otp_ver_tab_re').modal('hide');
            $('#step_1').hide();
             $('#step_2').hide();
             $('#step_3').hide();
             $('#step_5').hide();
             $('#step_2_re').hide();

             $('#step_4').hide();
             $('#step_4_re').show();
             $('html,body').scrollTop(0);
          //$('#department_list_show').hide();
        }else{
           $('#verify_failed_re').css('display','block');
           $('#sent_failed_re').css('display','none');
        }
      }
      });

      
   
     }else{
      $('#sent_failed_re').css('display','block');
     }
    });

   $('#submit_otp_2').on('click',function(){
     var get_otp_mail= $('#type_otp_mail').val();
     var get_otp= $('#type_otp_2').val();
     //alert(get_otp_mail);
     //alert(otp);
     if(get_otp_mail == mail_otp && get_otp == otp){

      $('#sent_failed_mail').css('display','none');
      $('#otp_ver_tab_level_2').modal('hide');
      $('#step_1').hide();
       $('#step_2').hide();
       $('#step_3').hide();
       $('#step_5').hide();
       $('#step_2_re').hide();
       $('#step_4_re').hide();
       $('#step_4').show();
       $('html,body').scrollTop(0);
   
     }else{
      $('#sent_failed_mail').css('display','block');
      $('#sent_success_mail').css('display','none');
     }
    });

   $('#resend_otp').on('click',function(){
     var visitor_number= $('#visitor_phone').val();
      $.ajax({
      type: "POST",
      url: "<?php echo base_url('client/dashboard/send_sms_otp')?>",
      data:{visitor_number:visitor_number},
      success: function(data){
        if(data == 'no'){
          //$('#department_list_show').hide();
        }else{
           otp= data;
          alert(otp);
          $('#sent_failed').css('display','none');
          //$('#otp_ver_tab_level_1').modal('show');
        }
      }
      });
     
    });

   $('#resend_otp_2').on('click',function(){
     var visitor_number= $('#visitor_phone').val();
      $.ajax({
      type: "POST",
      url: "<?php echo base_url('client/dashboard/send_mail_otp')?>",
      data:{visitor_number:visitor_number},
      success: function(data){
        if(data == 'no'){
         // $('#department_list_show').hide();
        }else{
           mail_otp= data;
          //alert(mail_otp);
          $('#sent_failed_mail').css('display','none');
          $('#sent_success_mail').css('display','block');
        }
      }
      });
     
    });

    $('#step_3_back_btn').on('click',function(){
       $('#step_1').hide();
       $('#step_3').hide();
       $('#step_4').hide();
       $('#step_4_re').hide();
       $('#step_5').hide();
       $('#step_2_re').hide();
       $('#step_2').show();
       $('html,body').scrollTop(0);

   });


</script>
</body>
</html>

