
<?php
   require 'include/check_session.php';
?>
  <!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Bind Visitors | Form Automation</title>
  <?php 
  require_once 'include/header.php'; 
  ?>
</head>
<body class="hold-transition skin-blue-light" style="background: #ECF0F5">
<!-- Site wrapper -->
  <?php require_once 'include/navbar.php'; ?>

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h3>Setup Form Automation</h3>
    </div>
  </div>
</div>

<div class="box box-primary">
  </div>
<section id="step_1">
<div class="container">
  <div class="progress" style="height: 20px;">
  <div class="progress-bar progress-bar-striped active progress-bar-success" role="progressbar" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100" style="width: 33%">
    <span ><b>Step 1: Automate Form Fields</b></span>
  </div>
  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 33%">
    <span ><b>Step 2: Page Setting</b></span>
  </div>
  <div class="progress-bar progress-bar-danger  " role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 33%">
    <span ><b>Step 3: Lanch Automation</b></span>
  </div>
</div>
  <div class="row">
    <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h1 class="box-title">Check required form fields for your visitor management system</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body " style="padding: 30px; font-size: 18px;">
              <table class="table table-striped">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Form Fields</th>
                  
                  <th style="width: 30%">Mandatory Fields</th>
                </tr>
                <tr>
                  <td>1.</td>
                  <td>Visitor Name</td>
                  
                  <td><label><input type="checkbox" class="flat-red selected_fields" ></label></td>
                </tr>
                <tr>
                  <td>2.</td>
                  <td>Phone No </td>
                  
                  <td><label><input type="checkbox" id="" class="flat-red selected_fields"></label></td>
                </tr>
                <tr>
                  <td>3.</td>
                  <td>Email ID</td>
                  
                  <td><label><input type="checkbox" id="form_email" class="flat-red selected_fields"></label></td>
                </tr>
                <tr>
                  <td>4.</td>
                  <td>Date of birth</td>
                  
                  <td><label><input type="checkbox" id="form_dob" class="flat-red selected_fields"></label></td>
                </tr>
                <tr>
                  <td>5.</td>
                  <td> Govt ID</td>
                  
                  <td><label><input type="checkbox" id="form_govt" class="flat-red selected_fields"></label></td>
                </tr>
                
              </table>
              <hr>
              <button type="button" id="step_1_next_btn" class="btn btn-md btn-primary btn-block">Next</button>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
    </div>
    
</section>

<section id="step_2">
<div class="container">
  <div class="progress" style="height: 20px;">
  <div class="progress-bar progress-bar-striped active progress-bar-success" role="progressbar" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100" style="width: 33%">
    <span ><b>Step 1: Automate Form Fields</b></span>
  </div>
  <div class="progress-bar progress-bar-striped  progress-bar-success active" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 33%">
    <span ><b>Step 2: Page Setting</b></span>
  </div>
  <div class="progress-bar progress-bar-danger  " role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 33%">
    <span ><b>Step 3: Lanch Automation</b></span>
  </div>
</div>
  <div class="row">
    <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h1 class="box-title">Customized Form Page Layout</h1>
            </div>

            <div class="box-body">
              <h1 class="box-title">Automation name</h1>
              <input type="text" class="form-control" id="auto_name" name="" placeholder="Enter automation name">
              <hr>
            </div>

            <div class="box-body">
              <h1 class="box-title">Company profile picture</h1>
              <hr>
              <h5 class="lead"> Select File</h5> <br>
            <input type="file" name="select_file" id="select_file" ><span style="margin-left: 30px;" id="file_upload_error"></span><span style="margin-left: 30px; width: 500px; height: 50px;" id="file_upload_preview"></span><br>
            <p class="text-secondary">*Only gif,png,jpg,jpeg,pdf and doc type file allowed <br>*File size should not exceedd more than 5MB</p>
            
            </div>
            
            <!-- /.box-header -->
            <div class="box-body " style="padding: 30px; font-size: 18px;">
            </div>

            <div class="box-footer " >
              
            <div  style="padding: 10px;">
             <div class="float-right">
              <button id="step_2_back_btn" style="position: relative; padding: 5px 40px;" class="btn  " >Back</button>
              
              <a href="#" id="step_2_next_btn" style="position: relative; padding: 5px 40px;" class="btn btn-primary pull-right" >Next</a>
              </div>
             <div class="clearfix"></div>
            </div>
          </div>
          </div>
    </div>
  </div>
</div>

<!-- <div class="container">
  <div class="row">
    <div class="col-md-12">
          <div class="box">
           
            <div class="box-body">
              <h1 class="box-title">Form background theme</h1>
              <hr>
              <h5 class="lead"> Select File</h5> <br>
            <input type="file" name="select_file" id="select_file_2" ><span style="margin-left: 30px;" id="file_upload_error_2"></span><span style="margin-left: 30px;" id="file_upload_preview_2"></span><br>
            <p class="text-secondary">*Only gif,png,jpg,jpeg,pdf and doc type file allowed <br>*File size should not exceedd more than 5MB</p>
            
            </div>
            <!- /.box-header ->
            
        </div>
    </div>
  </div>
</div> -->

<script type="text/javascript">
$(document).ready(function(){
 $(document).on('change', '#select_file', function(){
  var name = document.getElementById("select_file").files[0].name;
  var form_data = new FormData();
  var ext = name.split('.').pop().toLowerCase();
  if(jQuery.inArray(ext, ['gif','png','jpg','jpeg','pdf','doc','docx']) == -1) 
  {
   //alert("Invalid Image File");
   $('#select_file').val("");
   $('#select_file').css('border','0.8px solid red');
   $('#file_upload_error').html("<label class='text-danger'>Invalid File type</label>");
   $('#file_upload_preview').html('');
  }
  var oFReader = new FileReader();
  oFReader.readAsDataURL(document.getElementById("select_file").files[0]);
  var f = document.getElementById("select_file").files[0];
  var fsize = f.size||f.fileSize;
  if(fsize > 5000000)
  {
   //alert("Image File Size is very big");
    $('#select_file').val("");
    $('#select_file').css('border','0.8px solid red');
    $('#file_upload_error').html("<label class='text-danger'>File Size is greater than 5 MB</label>");
    $('#file_upload_preview').html('');
  }
  else
  {
   form_data.append("select_file", document.getElementById('select_file').files[0]);
   $.ajax({
    url:"<?php echo base_url('home/file_upload_step_2_logo');?>",
    method:"POST",
    data: form_data,
    contentType: false,
    cache: false,
    processData: false, 
    beforeSend:function(){
     $('#file_upload_error').html("<label class='text-success'>File Uploading...</label>");
    },   
    success:function(data)
    {
    $('#select_file').css('border','0.8px solid green');
    $('#file_upload_error').html("<label class='text-success'>File Uploaded successfully.</label>");
    //$('#file_upload_preview').html('<embed src="<?php //echo base_url()?>uploaded_files/'+data+'" height="150" width="225"  />');
    $('#file_upload_preview').html('<div style="height:200px" class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item img-responsive"  src="<?php echo base_url()?>assets/img/client/'+data+'"></iframe></div>');
    
    }
   });
  }
 });

});
 /*$(document).on('change', '#select_file_2', function(){
  var name = document.getElementById("select_file_2").files[0].name;
  var form_data = new FormData();
  var ext = name.split('.').pop().toLowerCase();
  if(jQuery.inArray(ext, ['gif','png','jpg','jpeg','pdf','doc','docx']) == -1) 
  {
   //alert("Invalid Image File");
   $('#select_file_2').val("");
   $('#select_file_2').css('border','0.8px solid red');
   $('#file_upload_error_2').html("<label class='text-danger'>Invalid File type</label>");
   $('#file_upload_preview_2').html('');
  }
  var oFReader = new FileReader();
  oFReader.readAsDataURL(document.getElementById("select_file_2").files[0]);
  var f = document.getElementById("select_file_2").files[0];
  var fsize = f.size||f.fileSize;
  if(fsize > 5000000)
  {
   //alert("Image File Size is very big");
    $('#select_file_2').val("");
    $('#select_file_2').css('border','0.8px solid red');
    $('#file_upload_error_2').html("<label class='text-danger'>File Size is greater than 5 MB</label>");
    $('#file_upload_preview_2').html('');
  }
  else
  {
   form_data.append("select_file_2", document.getElementById('select_file_2').files[0]);
   $.ajax({
    url:"<?php //echo base_url('home/file_upload_step_2_theme');?>",
    method:"POST",
    data: form_data,
    contentType: false,
    cache: false,
    processData: false, 
    beforeSend:function(){
     $('#file_upload_error_2').html("<label class='text-success'>File Uploading...</label>");
    },   
    success:function(data)
    {
    $('#select_file_2').css('border','0.8px solid green');
    $('#file_upload_error_2').html("<label class='text-success'>File Uploaded successfully.</label>");
    //$('#file_upload_preview_2').html('<embed src="<?php //echo base_url()?>uploaded_files/'+data+'" height="150" width="225"  />');
    $('#file_upload_preview_2').html('<div style="height:200px" class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item img-responsive"  src="<?php //echo base_url()?>assets/img/client/'+data+'"></iframe></div>');
    
    }
   });
  }
 });*/

</script>
<hr>
 
</section>

<section id="step_3">


<div class="container">
  <div class="progress" style="height: 20px;">
  <div class="progress-bar progress-bar-striped active progress-bar-success" role="progressbar" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100" style="width: 33%">
    <span ><b>Step 1: Automate Form Fields</b></span>
  </div>
  <div class="progress-bar progress-bar-striped active progress-bar-success" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 33%">
    <span ><b>Step 2: Page Setting</b></span>
  </div>
  <div class="progress-bar progress-bar-striped active progress-bar-success  " role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 33%">
    <span ><b>Step 3: Lanch Automation</b></span>
  </div>
</div>
  <div class="row">
    <div class="col-md-12">
          <div class="box">
           
            <div class="box-body">
              <h1 class="box-title">Visitor Verification</h1>
              <hr>
              <h5 class="lead"> Select level of authentication  (any one)</h5> <br>
            
              <!-- /.box-header -->
            <div class="box-body " style="padding: 10px; font-size: 18px;">
              <table class="table table-striped" >
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Authentication Type</th>
                  
                  <th style="width: 30%">Selected Fields</th>
                </tr>
                <tr id="no_otp_section">
                  <td>1.</td>
                  <td>No authentication</td>
                  
                  <td><label><input type="checkbox" id="no_otp" class="flat-red selected_fields" ></label></td>
                </tr>
                <tr id="single_otp_section">
                  <td>2.</td>
                  <td>Single level authentication <span ><p class="lead" style="font-size: 15px;"> (SMS OTP verification only)</p></span></td>
                  
                  <td><label><input type="checkbox" id="single_otp" class="flat-red selected_fields"></label></td>
                </tr>
                <tr id="double_otp_section">
                  <td>3.</td>
                  <td>Two level authentication <span ><p class="lead" style="font-size: 15px;"> (SMS OTP verification and email OTP verification)</p></span</td>
                  <td><label><input type="checkbox" id="double_otp" class="flat-red selected_fields"></label></td>
                </tr>
              </table>
            </div>
            </div>
            <!-- /.box-header -->
            <div class="box-footer " >
              
            <div  style="padding: 10px;">
             <div class="float-right">
              <button id="step_3_back_btn" style="position: relative; padding: 5px 40px;" class="btn  " >Back</button>
              
              <a href="#" id="submit_automation" style="position: relative; padding: 5px 40px;" class="btn btn-success pull-right" >Setup Automation</a>
              </div>
             <div class="clearfix"></div>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
</section>

<!-- Button trigger modal -->

<!-- Modal -->
<div class="modal fade" id="automation_success" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Automation Complete</h4>
      </div>
      <div class="modal-body">
        <center><img class="img-responsive" style=" height: 150px;" src="<?php echo base_url('assets/img/bigo.gif');?>"></center>
      </div>
      <div class="modal-footer">
        
        <button type="button" id="launch_form" class="btn btn-success">Launch Now</button>
        <button type="button" id="back_to_dashboard" class="btn btn-primary">Go To Dashboard</button>
      </div>
    </div>
  </div>
</div>

<?php
require_once 'include/footer.php';
?>
<script type="text/javascript">
    

    $('#launch_form').on('click',function(){
     
      $.ajax({
      url:"<?php echo base_url('home/launch_model_id');?>",
      method:"POST",
      beforeSend:function(){
       //$('#file_upload_error').html("<label class='text-success'>File Uploading...</label>");
      },   
      success:function(data)
      {
        var value_modifier= data*48;
        window.location.href="<?php echo base_url('home/demo_launch/?demo=')?>"+value_modifier;
      }
     });
      
    });

    
  //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    $('document').ready(function(){
    
    $('#step_3').hide();
    $('#step_2').hide();

    $('#step_1_next_btn').on('click',function(){
      
    
       $('#step_1').hide();
       $('#step_3').hide();
       $('#step_2').show();
       
       $('html,body').scrollTop(0);

   });

    $('#step_2_next_btn').on('click',function(){
       $('#step_1').hide();
       $('#step_2').hide();
       $('#step_3').show();
       
       $('html,body').scrollTop(0);

   });

    $('#step_2_back_btn').on('click',function(){
       $('#step_1').show();
       $('#step_2').hide();
       $('#step_3').hide();
       
       $('html,body').scrollTop(0);

   });

    $('#step_3_back_btn').on('click',function(){
       $('#step_1').hide();
       $('#step_2').show();
       $('#step_3').hide();
       
       $('html,body').scrollTop(0);

   });

  $('#submit_automation').on('click',function(){
    
    if($('#form_email').is(':checked')){

      var form_email="Set";
    }else{
       var form_email="Unset";
    }
  if($('#form_dob').is(':checked')){
          var form_dob="Set";
        }else{
           var form_dob="Unset";
        }
  if($('#form_govt').is(':checked')){
          var form_govt="Set";
        }else{
           var form_govt="Unset";
        }
    if($('#no_otp').is(':checked')){
          var auth_level=0;
        }else if($('#single_otp').is(':checked')){
          var auth_level=1;
        }else if($('#double_otp').is(':checked')){
          var auth_level=2;
        }

   
   var auto_name= $('#auto_name').val();
    var logo_file_name= $('#select_file').val();
    $.ajax({
    
    url:"<?php echo base_url('home/save_automation');?>",
    method:"POST",
    data:{ form_email:form_email, form_dob:form_dob , form_govt:form_govt , auth_level:auth_level , logo_file_name:logo_file_name , auto_name:auto_name},
    beforeSend:function(){  
       $('#submit_automation').html("Saving please wait...");  
      }, 
    success:function(data)
    {
       $('#automation_success').modal('show');
       //window.location.href="<?php //echo base_url();?>index.php?/Welcome/landing";
    }
   })
      
   });



$('#no_otp_section').on('click',function(){
   // alert('no-otp');
     $('#no_otp').prop('checked', true);
     $('#single_otp').prop('checked', false);
     $('#double_otp').prop('checked', false);
  });
  if($('#form_email').is(':checked')){
     //alert('form email');
      var form_email="Set";
    }

      if($('#no_otp').is(':checked')){
        //alert('no-otp');
        $('#single_otp').prop('checked', false);
        $('#double_otp').prop('checked', false);
      }

      if($('#single_otp').prop('checked')){
        $('#no_otp').prop('checked', false);
        $('#double_otp').prop('checked', false);
      }

      if($('#double_otp').prop('checked')){
        $('#single_otp').prop('checked', false);
        $('#no_otp').prop('checked', false);
      }
  
});
</script>
</body>
</html>

