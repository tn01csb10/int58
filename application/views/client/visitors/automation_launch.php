
<?php
   require 'include/check_session.php';
?>
  <!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Bind Visitors | Visitor management dashboard</title>
  <?php 
  require_once 'include/header.php'; 
  ?>
  <style type="text/css">
    .head{
      background-color: #ECF0F5;
    }
  </style>
</head>
<body class="hold-transition skin-blue-light sidebar-mini">

<div class="wrapper">
<!-- Site wrapper -->
  <?php require_once 'include/navbar.php'; ?>
  <?php require_once 'include/asidebar.php'; ?>
  <!-- Site Carousel -->
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <div  style="margin-top: 0;">
      <?php
        if(isset($_GET['launch'])){
          $launch= $_GET['launch'];
          $launch_id= $launch/44444444;
          $client_id=($this->session->userdata['logged_in']['client_id']);
        ?>
        <div class="container-fluid" style="">
          <div class="row">
            <h4 class="lead">Visitor management dashboard</h4>
            
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Visitor records</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th class="head">Name</th>
                  <th class="head">Phone No</th>
                  <th class="head">Email ID</th>
                  <th class="head">Date of birth</th>
                  <th class="head">Government ID</th>
                  <th class="head">ID number</th>
                  <th class="head">Reason to visit</th>
                  <th class="head">Company visited</th>
                  <th class="head">Department visited</th>
                  <th class="head">Employee visited</th>
                  <th class="head">Check in time</th>
                  <th class="head">Check out time</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <?php
                  $get_page_details=$this->db->get_where('visitor_details',array('client_id' => $client_id, 'automation_id' => $launch_id ));
                  foreach ($get_page_details->result() as $row_data)
                  { 
                    $fname=$row_data->fname;
                    $lname=$row_data->lname;
                    $email=$row_data->email;
                    $phone=$row_data->phone;
                    $dob=$row_data->dob;
                    $govt_id=$row_data->govt_id;
                    $id_number=$row_data->id_number;
                    $reason_to_visit=$row_data->reason_to_visit;
                    $company_visit=$row_data->company_visit;
                    $dept_visit=$row_data->dept_visit;
                    $employee_visit=$row_data->employee_visit;
                    $check_in_time=$row_data->check_in_time;
                    $check_out_time=$row_data->check_out_time;
                 ?>
                  <td><?php echo $fname.' '.$lname ?></td>
                  <td><?php echo $phone ?></td>
                  <td><?php echo $email ?></td>
                  <td><?php echo $dob ?></td>
                  <td><?php echo $govt_id ?></td>
                  <td><?php echo $id_number ?></td>
                  <td><?php echo $reason_to_visit ?></td>
                  <td><?php echo $company_visit ?></td>
                  <td><?php echo $dept_visit ?></td>
                  <td><?php echo $employee_visit ?></td>
                  <td><?php echo $check_in_time  ?></td>
                  <td><?php echo $check_out_time  ?></td>
                </tr>
                <?php
                 } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th class="head">Name</th>
                  <th class="head">Phone No</th>
                  <th class="head">Email ID</th>
                  <th class="head">Date of birth</th>
                  <th class="head">Government ID</th>
                  <th class="head">ID number</th>
                  <th class="head">Reason to visit</th>
                  <th class="head">Company visited</th>
                  <th class="head">Department visited</th>
                  <th class="head">Employee visited</th>
                  <th class="head">Check in time</th>
                  <th class="head">Check out time</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          </div>
          
        </div>
      <?php
      }else{
        ?>
        <div class="container">
            <div class="row">
        <div class="col-md-12">
          <h1>Visitor management</h1>
        </div>
        </div><br><br>
        <div class="row" style="background: #EEF3F9; padding:60px;">
        <div class="col-md-12">
          <center><img src="<?php echo base_url();?>assets/img/landing-page.SVG" style="width: 150px; height: auto;" >
          <h2 class="lead text-bold">You haven't created any Visitor form yet</h2>
          <h5 class=" text-secondary">Automate visitor registration and manage them using automation tools!</h5><br>
          <a  href="<?php echo base_url('home/form_automation'); ?>" style="border:dashed #B3B5BB;background: #F8F9FA;color: #000;" class="btn btn-md btn-secondary text-bold ">+  Create Visitor Form Now</a>
          </center>
        </div>
       </div>
      </div>
        <?php
      }
      ?>
      

    </div>
  </div>
</div>
<?php
require_once 'include/footer.php';
?>
<script type="text/javascript">
   $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })

</script>
</body>
</html>

