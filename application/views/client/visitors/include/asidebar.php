
  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        
        <li class="active">
          <a href="<?php echo base_url('home/client_dashboard')?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-gear"></i> <span>Manage company listing</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('client/Dashboard/add_company')?>"><i class="fa fa-circle-o"></i> Add Company</a></li>
            <li><a href="<?php echo base_url('client/Dashboard/delete_company')?>"><i class="fa fa-circle-o"></i> Delete Company</a></li>
          </ul>
        </li>
        <li class="">
          <a href="<?php echo base_url('home/visitor_management')?>">
            <i class="fa fa-dashboard"></i> <span>Visitor management</span>
          </a>
        </li>
        <li class="treeview ">
          <a href="#">
            <i class="fa fa-gear"></i> <span>Manage Category</span>
             <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a  href="<?php echo base_url('admin/Dashboard/add_category')?>"><i class="fa fa-circle-o"></i> Add Category</a></li>
            <li><a href="<?php echo base_url('admin/Dashboard/delete_category')?>"><i class="fa fa-circle-o"></i> Delete Category</a></li>
            <li><a href="<?php echo base_url('admin/Dashboard/update_category')?>"><i class="fa fa-circle-o"></i> Update Category</a></li>
          </ul>
        </li>
        
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>