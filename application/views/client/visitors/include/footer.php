<footer class="" >
	<div class="pull-right hidden-xs">
	</div>
	<!--<strong>Copyright &copy; 2018 <a href="#">Shop Corner</a>.</strong> All rights reserved.-->
</footer>

<script type="text/javascript" src="<?php echo base_url();?>/assets/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>/assets/js/bootstrap.js"></script>
<!-- Select2 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>
<script src="<?php echo base_url();?>/assets/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url();?>/assets/js/jquery.inputmask.js"></script>
<script src="<?php echo base_url();?>/assets/js/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url();?>/assets/js/jquery.inputmask.extensions.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url();?>/assets/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>/assets/js/dataTables.bootstrap.min.js"></script>
<!-- date-range-picker -->
<script src="<?php echo base_url();?>/assets/js/moment.min.js"></script>
<script src="<?php echo base_url();?>/assets/js/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo base_url();?>/assets/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="<?php echo base_url();?>/assets/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="<?php echo base_url();?>/assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url();?>/assets/js/jquery-ui.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url();?>/assets/js/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?php echo base_url();?>/assets/js/icheck.min.js"></script>

<!-- FastClick -->
<script src="<?php echo base_url();?>/assets/js/fastclick.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>/assets/js/adminlte.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>/assets/js/demo.js"></script>
<!-- Bootstrap slider -->
<script src="<?php echo base_url();?>/assets/js/bootstrap-slider.js"></script>
<script>
  $(function () {
    /* BOOTSTRAP SLIDER */
    $('.slider').slider()
  })
</script>