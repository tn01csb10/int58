<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Registration Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php 
  require_once 'include/header.php'; 
  ?>
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo" style="margin-top: -50px;">
    <a href=""><b>Bind</b>Visitor</a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Register a new membership</p>

    <form  >
      <div class="form-group has-feedback">
        <input type="text" id="first_name" class="form-control" placeholder="First name">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="text" id="last_name" class="form-control" placeholder="Last name">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="email" id="client_email" class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="email" id="client_no" class="form-control" placeholder="Phone No">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" id="client_pass" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" id="retype_pass" class="form-control" placeholder="Retype password">
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> I agree to the <a href="#">terms</a>
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="button" id="client_register" class="btn btn-primary btn-block btn-flat">Register</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using
        Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign up using
        Google+</a>
    </div>

    <a href="<?php echo base_url('home/client_login');?>" class="text-center">I already have a membership</a>
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<?php
require_once 'include/footer.php';
?>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });

  $('#client_register').on('click',function(){
   var email=$('#client_email').val();
   var first_name=$('#first_name').val();
   var last_name=$('#last_name').val();
   var client_no=$('#client_no').val();
   var password=$('#client_pass').val();
   var retype_pass=$('#retype_pass').val();
   
   if(first_name== ''){
    alert('first_name');
   }else if(last_name== ''){
    alert('last_name');
   }else if(email == ''){
    alert('email');
   }else if(client_no == ''){
    alert('client_no');
   }else if(password == ''){
    alert('password');
   }else if(retype_pass == ''){
    alert('retype_pass');
   }else if(password != retype_pass){
    alert('error');
   }else{
    $.ajax({
      url:"<?php echo base_url('auth/client_registration'); ?>",
      method:"POST",
      data:{email:email, first_name:first_name, last_name:last_name, client_no:client_no, password:password},
      beforeSend:function(){
        $('#client_register').html('Please wait.....');
      },
      success:function(data)
        {
         alert(data);
          if(data == 'true'){

            $("#client_register").html('Registration Successful');
            window.location.href = "<?php echo base_url('home/client_dashboard') ?>" ;
          }else{
            $("#client_register").html('Register');
          }
        // alert(data);
        }
     })
   }
     
   });

</script>
</body>
</html>
