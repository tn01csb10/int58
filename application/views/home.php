
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Bind Visitors | Home</title>
  <?php 
  require_once 'include/header.php'; 
  ?>
</head>
<body class="hold-transition skin-blue-light"
>
<!-- Site wrapper -->
  <?php require_once 'include/navbar.php'; ?>

  <!-- Site Carousel -->
<!-- <div  style="margin-top: 0;">
  <div class="row">
    <div class="col-md-12">
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
          <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
          <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
        </ol>
        <div class="carousel-inner">
          <div class="item active">
            <img src="<?php //echo base_url()?>1.JPG" style="width: 100%;height: 280px;" alt="First slide">
            <div class="carousel-caption">
              First Slide
            </div>
          </div>
          <div class="item">
            <img src="<?php //echo base_url()?>2.JPG" style="width: 100%;height: 280px;" alt="Second slide">
            <div class="carousel-caption">
              Second Slide
            </div>
          </div>
          <div class="item">
            <img src="<?php //echo base_url()?>4.JPG" style="width: 100%;height: 280px;" alt="Third slide">
            <div class="carousel-caption">
              Third Slide
            </div>
          </div>
        </div>
        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
          <span class="fa fa-angle-left"></span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
          <span class="fa fa-angle-right"></span>
        </a>
      </div>
    </div>
  </div><br><br>
   -->
<div style="background: url(<?php echo base_url('assets/img/b1.jpg')?>) no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover; ">
  <div style="height: 600px;">
    <div style="padding: 50px; background-color: #000; opacity: 0.6; position: relative; top: 40%; text-align: center; color: #fff;">
      <h1>BIND VISITORS</h1>
      <h3>Smart way to manage visitor registration</h3>
      <p>Collect guest information, print badges, capture photos and sign documents.</p>
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col-md-12"><br>
      <center><h1 class="">MAIN FEATURES</h1></center>
    </div>
  </div><br><br>
  <div class="row" style="padding: 40px;">
    <div class="col-md-4" >
      <div class="row">
        <div class="col-md-1">
          <i class="fa fa-check-circle-o" style="font-size: 25px;"></i>
        </div>
        <div class="col-md-11">
          <p class="lead">SMOOTH DESKTOP CHECK-IN</p>
          <p>As they enter a company’s lobby, visitors will see a desktop , which they can use to check in.</p>
          <a href="#">Learn more...</a>
        </div>
      </div>
    </div>
    <div class="col-md-4" >
      <div class="row">
        <div class="col-md-1">
          <i class="fa fa-check-circle-o" style="font-size: 25px;"></i>
        </div>
        <div class="col-md-11">
          <p class="lead">SMOOTH DESKTOP CHECK-IN</p>
          <p>The user is notified immediately by text message or email.</p>
          <a href="#">Learn more...</a>
        </div>
      </div>
    </div>
    <div class="col-md-4" >
      <div class="row">
        <div class="col-md-1">
          <i class="fa fa-check-circle-o" style="font-size: 25px;"></i>
        </div>
        <div class="col-md-11">
          <p class="lead">SMOOTH DESKTOP CHECK-IN</p>
          <p>A wireless label printer prints a badge for the visitor to identify him/her.</p>
          <a href="#">Learn more...</a>
        </div>
      </div>
    </div>
  </div>
  <div class="row" style="padding: 40px;">
    <div class="col-md-4" >
      <div class="row">
        <div class="col-md-1">
          <i class="fa fa-check-circle-o" style="font-size: 25px;"></i>
        </div>
        <div class="col-md-11">
          <p class="lead">DETAILED VISITOR LOGS</p>
          <p>View an organized, accurate list of visitor traffic, complete with check-in time, photo, ...</p>
          <a href="#">Learn more...</a>
        </div>
      </div>
    </div>
    <div class="col-md-4" >
      <div class="row">
        <div class="col-md-1">
          <i class="fa fa-check-circle-o" style="font-size: 25px;"></i>
        </div>
        <div class="col-md-11">
          <p class="lead">ON-SITE LEGAL DOCUMENT SIGNING</p>
          <p>When you use application name; document signing feature, visitors will read and sign your Terms on the computer.</p>
          <a href="#">Learn more...</a>
        </div>
      </div>
    </div>
    
  </div>
</div>

<div class="container-fluid" style="padding: 70px; background-color: #222D32; color: #fff;">
  <div class="row">
    <div class="col-md-3">
      <h1>Bind Visitors</h1>
      <p>Redefining enterprise security and safety through technology.</p>
    </div>
    <div class="col-md-3">
      <p>SECURE VISITOR MANAGEMENT</p>
      <ul>
        <li>BindVisitors for Government</li>
         <li>BindVisitors for Office</li>
          <li>BindVisitors for Manufacturing</li>
           <li>BindVisitors for education</li>
            <li>BindVisitors for Airports</li>
      </ul>
    </div>
    <div class="col-md-3">
      <p>RESOURCES</p>
      <ul class="ul">
        <li>Features & Options</li>
        <li>Pricing Plans</li>
        <li>Integrations</li>
        <li>Frequently Asked Questions</li>
      </ul>
    </div>
    <div class="col-md-3">
      <p>GET IN TOUCH</p>
      <i class="fa fa-phone">&nbsp; 1-855-5-BVISITOR</i>
      <br>
      <i class="fa fa-paper-plane">&nbsp; sales@goilobby.com</i>
       <br>
      <i class="fa  fa-location-arrow">&nbsp; 3605 Weston Rd</i>
    </div>
  </div>
</div>

    <div class="modal fade" id="modal-default">
          <div class="modal-dialog modal-md">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Login Here</h4>
              </div>
              <div class="modal-body">
                <form class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Email</label>

                  <div class="col-sm-10">
                    <input type="email" class="form-control" id="client_email" placeholder="Email">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Password</label>

                  <div class="col-sm-10">
                    <input type="password" class="form-control" id="client_pass" placeholder="Password">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox"> Remember me
                      </label>
                      <span class="pull-right"><a href="<?php echo base_url('home/client_registration')?>"> New user? Register here </a></span>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-default">Cancel</button>
                <a  id="client_login" class="btn btn-social btn-primary pull-right"><i class="fa fa-sign-in "></i>Login</a>
              </div>
              <!-- /.box-footer -->
            </form>
              </div>
              
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

<?php
require_once 'include/footer.php';
?>
<script type="text/javascript">
  $('#client_login').on('click',function(){
   var username=$('#client_email').val();
   var password=$('#client_pass').val();
   
     $.ajax({
      url:"<?php echo base_url('auth/verify_client_login'); ?>",
      method:"POST",
      data:{username:username, password:password},
      beforeSend:function(){
        $('#client_login').html('Please wait.....');
      },
      success:function(data)
        {
          if(data == 'true'){
            $("#client_login").html('Login Successful');
            window.location.href = "<?php echo base_url('home/client_dashboard') ?>" ;
          }else if(data == 'false'){
            $("#client_login").html('Login');
          }else{
            $("#client_login").html('Login');
          }
        // alert(data);
        }
     })
   });

</script>
</body>
</html>
