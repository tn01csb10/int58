
<header class="main-header" >
    <!-- Logo -->
    <a href="" class="logo" >
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>S</b>hop</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Bind</b>Visitor</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top " >
       
      <!-- Sidebar toggle button-->
      <div class="navbar-custom-menu">

        <ul class="nav navbar-nav">
          
          <li class="dropdown tasks-menu">
            <a href="#" >
              <span class="" data-toggle="modal" data-target="#modal-default" >Login  </span>
            </a>
          </li>
          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="">More </span>
              <i class="fa fa-angle-down"></i>
            </a>
          </li>
          
        </ul>
      </div>
    </nav>
  </header>
  
  
<script type="text/javascript">
  $(document).ready(function(){
    $(".dropdown, .btn-group").hover(function(){
        var dropdownMenu = $(this).children(".dropdown-menu");
        if(dropdownMenu.is(":visible")){
            dropdownMenu.parent().toggleClass("open");
        }
    });
  });
</script>
